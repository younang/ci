module.exports = function(config) {
	"use strict";

	config.set({

		frameworks: ["ui5"],

		preprocessors: {
			"{webapp,webapp/!(test)}/*.js": ["coverage"]
		},

		coverageReporter: {
			includeAllSources: true,
			reporters: [
				{
					type: "html",
					dir: "coverage"
				},
				{
					type: "text"
				}
			],
			check: {
				each: {
					statements: 100,
					branches: 100,
					functions: 100,
					lines: 100
				}
			}
		},

		reporters: ["progress", "coverage"],

		browsers: ["CustomChromeHeadless"],
		customLaunchers: {
            CustomChromeHeadless: {
                base: 'ChromeHeadless',
                flags: ['--no-sandbox']
            }
        },

		browserConsoleLogOptions: {
			level: "error"
        },

		singleRun: true
	});
};