sap.ui.define([
		"sap/ui/core/mvc/Controller"
	],
	/**
     * @param {typeof sap.ui.core.mvc.Controller} Controller
     */
	function (Controller) {
		"use strict";

		return Controller.extend("com.concepit.web.controller.View", {
			onInit: function () {
                this._check();
            },
            _check: function(param){
                var result;
                param ? result = true : result = false;
            }
		});
	});
